package org.nachots.crypto3_8;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.nachots.crypto3_8.aes.AESCipher;
import org.nachots.cryptoutils.CryptoUtils;

/**
 * Exercise 3.8 main class
 * @author nachots
 * @version 1.0
 */
public class CryptoExercise3_8 {
	
	public static void main(String[] args) {
		System.out.println("---Exercise 3.8---");
		System.out.println();
		
		final byte[] cipherText = {0x53, (byte) 0x9b, 0x33, 0x3b, 0x39, 0x70, 0x6d, 0x14,
									(byte) 0x90, 0x28, (byte) 0xcf, (byte) 0xe1, (byte) 0xd9, (byte) 0xd4, (byte) 0xa4, 0x07};
		
		final byte[] key = {(byte) 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
								0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01};
		
		byte[] plaintext = null;
		try {
			plaintext = AESCipher.decipher(cipherText, key);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		
		System.out.println("Ciphertext:");
		CryptoUtils.printByteArray(cipherText, 16);
		System.out.println("Key:");
		CryptoUtils.printByteArray(key, 16);
		System.out.println("Plaintext:");
		CryptoUtils.printByteArray(plaintext, 16);
	}

}
