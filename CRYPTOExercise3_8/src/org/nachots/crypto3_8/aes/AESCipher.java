package org.nachots.crypto3_8.aes;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * AES cipher helper class.
 * @author nachots
 * @version 1.0
 */
public class AESCipher {
	
	/**
	 * Deciphers a 128 bit ciphertext with a 256 bit AES key using AES/ECB/NoPadding.
	 * @param ciphertext The 128-bit ciphertext to be deciphered.
	 * @param key The 256-bit DES key used to decipher the ciphertext.
	 * @return The plaintext resulted of applying the AES decipher function to the given ciphertext and key.
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static byte[] decipher(byte[] ciphertext, byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher aes = Cipher.getInstance("AES/ECB/NoPadding");
		aes.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, "AES"));
		return aes.doFinal(ciphertext);
	}

}
