package org.nachots.crypto3_10;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.nachots.crypto3_10.des.DESCipher;

import static org.nachots.cryptoutils.CryptoUtils.complementOf;
import static org.nachots.cryptoutils.CryptoUtils.toHexString;


/**
 * Exercise 3.10 main class
 * @author nachots
 * @version 1.0
 */
public class CryptoExercise3_10 {

	private static final int DES_BLOCK_SIZE  = 8; // 64 bits
	private static final int DES_KEY_SIZE = 8; // 56 bits + 8 bits of parity
	
	public static void main(String[] args) {
		// Generate plaintext and key
		SecureRandom random = new SecureRandom();
		byte[] plaintext = new byte[DES_BLOCK_SIZE];
		byte[] key = new byte[DES_KEY_SIZE];
		random.nextBytes(plaintext);
		random.nextBytes(key);
		
		// Cipher plaintext with key
		byte[] ciphertext = null;
		try {
			ciphertext = DESCipher.cipher(plaintext, key);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			e.printStackTrace();
		}
		
		// Complement of plaintext of key, and complement of its ciphertext
		byte[] plaintextComplement = complementOf(plaintext);
		byte[] keyComplement = complementOf(key);
		byte[] ciphertextComplement = null;
		try {
			ciphertextComplement = complementOf(DESCipher.cipher(plaintextComplement, keyComplement));
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			e.printStackTrace();
		}
		
		System.out.println(String.format("P  %s", toHexString(plaintext, 8)));
		System.out.println(String.format("K  %s", toHexString(key, 8)));
		System.out.print(String.format("C  %s", toHexString(ciphertext, 8)));
		System.out.println("-------------");
		System.out.println(String.format("~P %s", toHexString(plaintextComplement, 8)));
		System.out.println(String.format("~K %s", toHexString(keyComplement, 8)));
		System.out.print(String.format("~C %s", toHexString(ciphertextComplement, 8)));
		System.out.println("-------------");
		if (Arrays.equals(ciphertext, ciphertextComplement)) {
			System.out.println("For this key and plaintext, E(K,P)=~E(~K,~P)");
		} else {
			System.out.println("E(K,P)=~E(~K,~P) is not true for this key and plaintext.");
		}
	}

}
