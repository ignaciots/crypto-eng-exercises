package org.nachots.crypto3_10.des;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * DES cipher helper class.
 * @author nachots
 * @version 1.0
 */
public class DESCipher {
	
	/**
	 * Ciphers a 64 bit plaintext with a 64 bit DES key using DES/ECB/NoPadding.
	 * @param plaintext The 64-bit plaintext to be ciphered.
	 * @param key The 64-bit DES key used to cipher the plaintext.
	 * @return The ciphertext resulted of applying the DES cipher function to the given plaintext and key.
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static byte[] cipher(byte[] plaintext, byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher des = Cipher.getInstance("DES/ECB/NoPadding");
		des.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "DES"));
		return des.doFinal(plaintext);
	}

}
