package org.nachots.crypto9_5.csprng;

import java.security.SecureRandom;

public class CSPRNG {
	
	private static final long SET_MAX_VALUE = (long)1<<32;
	private static final int MAX_NUMBER_OF_BITS = 32;
	private static final int BYTE_VALUE = 8;
	
	private final SecureRandom random;
	
	public CSPRNG() {
		random = new SecureRandom();
	}
	
	
	/**
	 * Returns a 32 bit random number in the set [0, n-1].
	 * @param n A number between 1 and 2**32.
	 * @return A 32 bit random number in the set [0, n-1]
	 */
	public long randomInSet(long n) {
		if (n < 1 || n > SET_MAX_VALUE) {
			throw new IllegalArgumentException("n must be in the range [1, 2**32]");
		}
		
		int k = minNumberOfBitsFor(n);
		long K = 0;
		do {
			K = kBitRandomNumber(k);
		} while (K >= n);
		
		return K;
	}
	
	private int minNumberOfBitsFor(long n) {
		for (int i = 2; i < MAX_NUMBER_OF_BITS; i++) {
			long minBitNumber = 1 << i;
			if (minBitNumber >= n) {
				return i;
			}
		}
		
		return MAX_NUMBER_OF_BITS;
	}
	
	private long kBitRandomNumber(int k) {
		if (k < BYTE_VALUE) {
			byte[] randomNumberArray = new byte[1];
			random.nextBytes(randomNumberArray);
			long randomNumber = randomNumberArray[0] >> (BYTE_VALUE - k);
			return randomNumber;
		}
		
		if (k % BYTE_VALUE == 0) {
			int numberOfBytes = k / BYTE_VALUE;
			byte[] randomNumberArray = new byte[numberOfBytes];
			random.nextBytes(randomNumberArray);
			
			long randomNumber = 0;
			for (int i = 0; i < randomNumberArray.length; i++) {
				randomNumber += (randomNumberArray[i] << (i*8));
			}
			return randomNumber;
		}
		
		int numberOfBytes = k / BYTE_VALUE + 1;
		byte[] randomNumberArray = new byte[numberOfBytes];
		random.nextBytes(randomNumberArray);
		
		long randomNumber = 0;
		int lastIndex = randomNumberArray.length - 1;
		for (int i = 0; i < lastIndex; i++) {
			randomNumber += (randomNumberArray[i] << (i*8));
		}
		
		long lastOctet = randomNumberArray[lastIndex] >> (numberOfBytes - k);
		randomNumber += lastOctet << (lastIndex * 8);
		
		return randomNumber;
	}
	
	

}
