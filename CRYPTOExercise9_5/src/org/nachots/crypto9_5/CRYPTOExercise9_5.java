package org.nachots.crypto9_5;

import org.nachots.crypto9_5.csprng.CSPRNG;

/**
 * Cryptographic Engineering exercise 9.2
 * @author nachots
 * @version 1.0
 */
public class CRYPTOExercise9_5 {
	
	public static void main(String[] args) {
		System.out.println("---Exercise 9.5---");
		
		CSPRNG rng = new CSPRNG();
		
		long fourBitNumber = rng.randomInSet(4);
		long sixteenBitNumber = rng.randomInSet(16);
		long twentyFourBitNumber = rng.randomInSet(24);
		
		System.out.println(fourBitNumber);
		System.out.println(sixteenBitNumber);
		System.out.println(twentyFourBitNumber);
		
	}

}
