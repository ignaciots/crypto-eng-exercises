package org.nachots.crypto4_5.aes;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * AES cipher helper class.
 * @author nachots
 * @version 1.0
 */
public class AESCipher {
	
	/**
	 * Ciphers a plaintext with a 256 bit AES key using AES/ECB/NoPadding.
	 * @param plaintext The 128-bit plaintext to be ciphered.
	 * @param key The 256-bit DES key used to cipher the plaintexttext.
	 * @return The ciphertext resulted of applying the AES cipher function to the given plaintext and key.
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static byte[] cipher(byte[] plaintext, byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher aes = Cipher.getInstance("AES/ECB/NoPadding");
		aes.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES"));
		return aes.doFinal(plaintext);
	}

}
