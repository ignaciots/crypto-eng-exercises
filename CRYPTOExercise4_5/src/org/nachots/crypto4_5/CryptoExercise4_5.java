package org.nachots.crypto4_5;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.nachots.crypto4_5.aes.AESCipher;
import org.nachots.cryptoutils.CryptoUtils;

/**
 * Exercise 4.5 main class
 * @author nachots
 * @version 1.0
 */
public class CryptoExercise4_5 {
	
	public static void main(String[] args) {
		System.out.println("---Exercise 4.5---");
		System.out.println();
		
		final byte[] plaintext = {0x62, 0x6c, 0x6f, 0x63, 0x6b, 0x20, 0x63, 0x69, 0x70, 0x68, 0x65, 0x72, 0x73, 0x20, 0x20, 0x20,
									0x68, 0x61, 0x73, 0x68, 0x20, 0x66, 0x75, 0x6e, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x20, 0x78,
									0x62, 0x6c, 0x6f, 0x63, 0x6b, 0x20, 0x63, 0x69, 0x70, 0x68, 0x65, 0x72, 0x73, 0x20, 0x20, 0x20};
		
		final byte[] key = {(byte) 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
								0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01};
		
		System.out.println("Plaintext:");
		CryptoUtils.printByteArray(plaintext, 16);
		System.out.println("Plantext (text):");
		CryptoUtils.printByteText(plaintext);
		System.out.println();
		System.out.println("Key:");
		CryptoUtils.printByteArray(key, 16);
		
		byte[] ciphertext = null;
		try {
			ciphertext = AESCipher.cipher(plaintext, key);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		
		
		System.out.println("Ciphertext:");
		CryptoUtils.printByteArray(ciphertext, 16);
	}

}
