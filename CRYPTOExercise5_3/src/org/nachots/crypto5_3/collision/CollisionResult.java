package org.nachots.crypto5_3.collision;

import static org.nachots.cryptoutils.CryptoUtils.toHexString;

/**
 * Collision result wrapper
 * @author nachots
 * @version 1.0
 */
public class CollisionResult {
	
	private final byte[] message1;
	private final byte[] message2;
	private final byte[] collisionHash;
	
	public CollisionResult(byte[] message1, byte[] message2,
			byte[] collisionHash) {
		this.message1 = message1;
		this.message2 = message2;
		this.collisionHash = collisionHash;
	}

	public byte[] getMessage1() {
		return message1;
	}

	public byte[] getMessage2() {
		return message2;
	}

	public byte[] getCollisionHash() {
		return collisionHash;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CollisionResult [message1=");
		builder.append(toHexString(message1, 8));
		builder.append(", message2=");
		builder.append(toHexString(message2, 8));
		builder.append(", collisionHash=");
		builder.append(toHexString(collisionHash, 8));
		builder.append("]");
		return builder.toString();
	}

}
