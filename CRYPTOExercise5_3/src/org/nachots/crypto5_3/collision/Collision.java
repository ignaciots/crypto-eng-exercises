package org.nachots.crypto5_3.collision;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.nachots.crypto5_3.sha512n.SHA512N;
import org.nachots.cryptoutils.CryptoUtils;
import org.nachots.cryptoutils.DataGenerator;

import static org.nachots.cryptoutils.CryptoUtils.toByteArray;

/**
 * Class used to seek collitions in SHA-512-n function.
 * @author nachots
 * @version 1.0
 */
public class Collision {

	/**
	 * Seeks a collision for SHA-512-n hash.
	 * @param n the number of bits in SHA-512-n.
	 * @return a {@link CollisionResult} with the data of the collision.
	 * @throws NoSuchAlgorithmException thrown by {@link SHA512N}.
	 */
	public static CollisionResult findSHA512CollisionFor(int n) throws NoSuchAlgorithmException {
		
		// Map of hashed inputs and message inputs
		DataGenerator generator = CryptoUtils.getDataGenerator();
		Map<Integer, Byte[]> hashedInputs = new HashMap<Integer, Byte[]>();
		boolean foundCollision = false;
		CollisionResult result = null;
		while (!foundCollision) {
			// get the next sequential data and hash it
			byte[] nextMessage = generator.getNextData();
			byte[] hashedMessage = SHA512N.sha512N(nextMessage, n);
			
			// if it is already in the hashes, we found a collision and set the result.
			if (hashedInputs.containsKey(Arrays.deepHashCode(toByteArray(hashedMessage)))) {
				foundCollision = true;
				result = new CollisionResult(nextMessage, toByteArray(hashedInputs.get(Arrays.deepHashCode(toByteArray(hashedMessage)))), hashedMessage);
			} else {
			// if not, add the hashed message to the hashed inputs.
				hashedInputs.put(Arrays.deepHashCode(toByteArray(hashedMessage)), toByteArray(nextMessage));
			}
		}
		
		return result;
	}

}
