package org.nachots.crypto5_3;

import java.security.NoSuchAlgorithmException;

import org.nachots.crypto5_3.collision.CollisionResult;
import org.nachots.crypto5_3.collision.Collision;

/**
 * Cryptography Engineering 5.3 exercise
 * @author nachots
 * @version 1.0
 */
public class CryptoExercise5_3 {
	
	private static final int NUMBER_OF_RUNS = 5; // number of runs for each SHA-512-N variation

	public static void main(String[] args) {
		cryptoExercise5_3();
	}
	
	private static void cryptoExercise5_3() {
		System.out.println("---Exercise 5.3---");
		System.out.println();
		
		// Test collision for SHA-512-8
		System.out.println("SHA-512-8");
		testSHA512CollisionFor(8);
		System.out.println();
		
		// Test collision for SHA-512-16
		System.out.println("SHA-512-16");
		testSHA512CollisionFor(16);
		System.out.println();
				
		// Test collision for SHA-512-24
		System.out.println("SHA-512-24");
		testSHA512CollisionFor(24);
		System.out.println();
				
		// Test collision for SHA-512-32
		System.out.println("SHA-512-32");
		testSHA512CollisionFor(32);
		System.out.println();
		
		// Test collision for SHA-512-40
		System.out.println("SHA-512-40");
		testSHA512CollisionFor(40);
		System.out.println();
		
		// Test collision for SHA-512-48
		System.out.println("SHA-512-48");
		testSHA512CollisionFor(48);
		System.out.println();
	}
	
	/**
	 * Test for SHA-512-n collision
	 * @param n The number of bits in the SHA-512-n function.
	 */
	private static void testSHA512CollisionFor(int n) {
		long totalTime = 0;
		for (int i = 0; i < NUMBER_OF_RUNS; i++) {
			long initTime = System.currentTimeMillis();
			CollisionResult result = null;
			try {
				result = Collision.findSHA512CollisionFor(n);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			long endTime = System.currentTimeMillis();
			long currentTime = (endTime - initTime);
			totalTime += currentTime;
			System.out.println("Collision found in " + currentTime + " ms.");
			System.out.println(result);
		}
		
		long averageTime = totalTime / NUMBER_OF_RUNS;
		System.out.println("Average time for " + NUMBER_OF_RUNS + " runs is " + averageTime + " ms.");
	} 
}
