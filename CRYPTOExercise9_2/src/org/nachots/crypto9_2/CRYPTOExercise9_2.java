package org.nachots.crypto9_2;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.nachots.cryptoutils.CryptoUtils;

/**
 * Cryptographic Engineering exercise 9.2
 * @author nachots
 * @version 1.0
 */
public class CRYPTOExercise9_2 {
	
	public static void main(String[] args) {
		System.out.println("---Exercise 9.2---");
		
		// Pick the algorithm.
		SecureRandom random = null;
		try {
			random = SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		// Print the chosen secure algorithm.
		System.out.println(String.format("Chosen %s as the random algorithm.", random.getAlgorithm()));
		
		byte[] AESKey = new byte[32];
		random.nextBytes(AESKey);
		
		System.out.println("Generated AES key (256 bits):");
		CryptoUtils.printByteArray(AESKey, 8);
		
	}

}
