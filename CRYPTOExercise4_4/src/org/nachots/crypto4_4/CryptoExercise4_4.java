package org.nachots.crypto4_4;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.nachots.crypto4_4.aes.AESCipher;
import org.nachots.cryptoutils.CryptoUtils;

/**
 * Exercise 4.4 main class
 * @author nachots
 * @version 1.0
 */
public class CryptoExercise4_4 {
	
	public static void main(String[] args) {
		System.out.println("---Exercise 4.4---");
		System.out.println();
		
		final byte[] iv = {(byte) 0x87, (byte) 0xf3, 0x48, (byte) 0xff, 0x79, (byte) 0xb8, 0x11, (byte) 0xaf, 0x38, 0x57, (byte) 0xd6, 0x71, (byte) 0x8e, 0x5f, 0x0f, (byte) 0x91};
		
		final byte[] ciphertext = {0x7c, (byte) 0x3d,(byte) 0x26, (byte) 0xf7, (byte) 0x73, (byte) 0x77, (byte) 0x63, (byte) 0x5a,
									(byte) 0x5e, (byte) 0x43, (byte) 0xe9, (byte) 0xb5, (byte) 0xcc, (byte) 0x5d, (byte) 0x05, (byte) 0x92,
									0x6e, 0x26, (byte) 0xff, (byte) 0xc5, 0x22, 0x0d, (byte) 0xc7, (byte) 0xd4, 0x05, (byte) 0xf1, 0x70, (byte) 0x86, 0x70, (byte) 0xe6, (byte) 0xe0, 0x17};
		
		final byte[] key = {(byte) 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
								0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01};
		
		byte[] plaintext = null;
		try {
			plaintext = AESCipher.decipher(ciphertext, key, iv);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		}
		
		System.out.println("Ciphertext:");
		CryptoUtils.printByteArray(ciphertext, 16);
		System.out.println("Key:");
		CryptoUtils.printByteArray(key, 16);
		System.out.println("IV: ");
		CryptoUtils.printByteArray(key, 16);
		System.out.println("Plaintext:");
		CryptoUtils.printByteArray(plaintext, 16);
		System.out.println("Plantext (text):");
		CryptoUtils.printByteText(plaintext);
	}

}
