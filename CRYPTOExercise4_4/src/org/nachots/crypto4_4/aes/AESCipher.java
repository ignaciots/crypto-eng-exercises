package org.nachots.crypto4_4.aes;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * AES cipher helper class.
 * @author nachots
 * @version 1.0
 */
public class AESCipher {
	
	/**
	 * Deciphers a ciphertext with a 256 bit AES key using AES/CBC with a given IV.
	 * @param cipher ciphertext to be deciphered.
	 * @param key The 256-bit AES key used to decipher the ciphertext.
	 * @return The plaintext resulted of applying the AES decipher function to the given ciphertext and key.
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidAlgorithmParameterException 
	 */
	public static byte[] decipher(byte[] ciphertext, byte[] key, byte[] iv) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
		Cipher aes = Cipher.getInstance("AES/CBC/NoPadding");
		IvParameterSpec ivSpec = new IvParameterSpec(iv);
		aes.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, "AES"), ivSpec);
		return aes.doFinal(ciphertext);
	}

}
