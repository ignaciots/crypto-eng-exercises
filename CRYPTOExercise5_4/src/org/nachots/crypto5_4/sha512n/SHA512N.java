package org.nachots.crypto5_4.sha512n;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * SHA-512-n algorithm implementation.
 * Compute SHA-512 and truncate the hash by getting the n first bits.
 * @author nachots
 * @version 1.0
 */
public class SHA512N {
	
	// Used algorithm
	private static final String DIGEST_ALGORITHM = "SHA-512";
	
	// Exception string messages
	private static final String ILLARG_SHA_MSG_NULL = "The message must not be null";
	private static final String ILLARG_SHA_N_MOD = "n must be a multiple of 8.";
	private static final String ILLARG_SHA_N_VAL = "n must be a value between 8 and 48.";
	
	/**
	 * Calculates the SHA-512 hash of a message and truncates it by taking its n first bits.
	 * @param message the message used in the SHA-512-N calculation.
	 * @param n the number of bits used in the truncation, a number divisible by 8 and between 8 and 48
	 * @return an array resulting of applying SHA-512-N to the message.
	 * @throws NoSuchAlgorithmException when the program cannot locate the SHA-512 algorithm necessary to implement SHA-512-N.
	 */
	public static byte[] sha512N(byte[] message, int n) throws NoSuchAlgorithmException {
		handleParameterErrors(message, n);
		
		MessageDigest sha512 = MessageDigest.getInstance(DIGEST_ALGORITHM);
		final byte[] digest = sha512.digest(message);
		
		byte[] truncatedDigest = truncateDigest(digest, n);
		return truncatedDigest;
	}

	private static void handleParameterErrors(byte[] message, int n) {
		if (message == null) {
			throw new IllegalArgumentException(ILLARG_SHA_MSG_NULL);
		}
		
		if (n < 8 || n > 48) {
			throw new IllegalArgumentException(ILLARG_SHA_N_VAL);
		}
		
		if (n % 8 != 0) {
			throw new IllegalArgumentException(ILLARG_SHA_N_MOD);
		}
	}

	/**
	 * Truncates a digest by taking the first n bits of it and discarding the rest.
	 * @param digest the digest to be truncated.
	 * @param n the number of bits used in the truncation.
	 * @return the truncated digest.
	 */
	private static byte[] truncateDigest(byte[] digest, int n) {
		int nBytes = n / 8;
		byte[] truncatedDigest = new byte[nBytes];
		for (int i = 0; i < nBytes; i++) {
			truncatedDigest[i] = digest[i];
		}
		
		return truncatedDigest;
	}
}
