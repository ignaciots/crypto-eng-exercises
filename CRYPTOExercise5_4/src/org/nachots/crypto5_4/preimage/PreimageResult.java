package org.nachots.crypto5_4.preimage;

import static org.nachots.cryptoutils.CryptoUtils.toHexString;

/**
 * Preimage result wrapper
 * @author nachots
 * @version 1.0
 */
public class PreimageResult {
	
	private final byte[] hash;
	private final byte[] preimage;
	
	public PreimageResult(byte[] preimage, byte[] hash) {
		this.preimage = preimage;
		this.hash = hash;
	}
	
	public byte[] getHash() {
		return hash;
	}
	
	public byte[] getPreimage() {
		return preimage;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PreimageResult [preimage=");
		builder.append(toHexString(preimage, 8));
		builder.append(", hash=");
		builder.append(toHexString(hash, 8));
		builder.append("]");
		return builder.toString();
	}
	
	

}
