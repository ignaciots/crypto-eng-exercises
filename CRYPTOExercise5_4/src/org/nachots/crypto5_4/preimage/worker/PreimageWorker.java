package org.nachots.crypto5_4.preimage.worker;

import static org.nachots.cryptoutils.CryptoUtils.toByteArray;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.nachots.crypto5_4.preimage.Preimage;
import org.nachots.crypto5_4.preimage.PreimageResult;
import org.nachots.crypto5_4.sha512n.SHA512N;
import org.nachots.cryptoutils.DataGenerator;

/**
 * Preimage worker used in {@link Preimage} to seek for preimages.
 * @author nachots
 * @version 1.0
 */
public class PreimageWorker implements Runnable {
	
	private final DataGenerator generator;
	private final int nBits;
	private final Byte[] hash;
	private final PreimageWorkerState state;
	
	/**
	 * Creates a new {@link PreimageWorker} that will calculate preimages according to the given data.
	 * @param generator The generator used to generate the brute-force numbers.
	 * @param nBits The number of bits used in the SHA-512-n function.
	 * @param hash The hash used to compute the preimage.
	 * @param state The state of the Preimage, common to all {@link PreimageWorker}.
	 */
	public PreimageWorker(DataGenerator generator, int nBits, Byte[] hash, PreimageWorkerState state) {
		this.generator = generator;
		this.nBits = nBits;
		this.hash = hash;
		this.state = state;
	}

	@Override
	public void run() {

		while (!state.foundPreimage()) {
			byte[] nextPreimageCandidate = generator.getNextData();
			byte[] preimageCandidateHash = null;
			try {
				preimageCandidateHash = SHA512N.sha512N(nextPreimageCandidate, nBits);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}

			// compare preimage candidate hash with given hash
			if (Arrays.deepEquals(toByteArray(preimageCandidateHash), hash)) {
				state.setPreimageResult(new PreimageResult(nextPreimageCandidate, preimageCandidateHash));
			}
		}

	}

}
