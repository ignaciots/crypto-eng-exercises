package org.nachots.crypto5_4.preimage.worker;

import org.nachots.crypto5_4.preimage.PreimageResult;

/**
 * Class representing the current preimage state, common to all running {@link PreimageWorker}.
 * @author nachots
 * @version 1.0
 */
public class PreimageWorkerState {
	
	private PreimageResult result;
	private Boolean foundPreimage;
	
	/**
	 * Creates a new empty {@link PreimageWorkerState}.
	 */
	public PreimageWorkerState() {
		this.result = null;
		this.foundPreimage = false;
	}

	/**
	 * Gets the {@link PreimageResult} obtained from the {@link PreimageWorker} calculations.
	 * @return A {@link PreimageResult} showing the obtained preimage data.
	 */
	public synchronized PreimageResult getPreimageResult() {
		return result;
	}

	/**
	 * Sets the {@link PreimageResult} and the foundPreimage flag-
	 * @param result The new {@link PreimageResult}.
	 */
	public synchronized void setPreimageResult(PreimageResult result) {
		this.result = result;
		this.foundPreimage = true;
	}

	/**
	 * Gets whether the preimage has already been found by a {@link PreimageWorker}.
	 * @return A {@link Boolean} indicating whether the preimage has already been found by a {@link PreimageWorker}.
	 */
	public synchronized Boolean foundPreimage() {
		return foundPreimage;
	}

}
