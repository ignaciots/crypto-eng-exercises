package org.nachots.crypto5_4.preimage;

import java.security.NoSuchAlgorithmException;
import org.nachots.crypto5_4.preimage.worker.PreimageWorker;
import org.nachots.crypto5_4.preimage.worker.PreimageWorkerState;
import org.nachots.cryptoutils.CryptoUtils;
import org.nachots.cryptoutils.DataGenerator;

import static org.nachots.cryptoutils.CryptoUtils.toByteArray;

/**
 * Class used to seek preimages in SHA-512-n function.
 * @author nachots
 * @version 1.0
 */
public class Preimage {

	/**
	 * Finds a preimage for a given hash using the SHA-512-n function.
	 * @param n The number of bits used in the SHA-512-n function.
	 * @param hash The hash used to compute the preimage.
	 * @return The preimage of the given hash.
	 * @throws NoSuchAlgorithmException 
	 */
	public static PreimageResult findSHA512PreimageFor(int n, byte[] hash) {
		final DataGenerator generator = CryptoUtils.getDataGenerator();
		final Byte[] hashByte = toByteArray(hash);
		
		PreimageWorkerState state = new PreimageWorkerState();
		
		Thread[] preimageWorkers = new Thread[Runtime.getRuntime().availableProcessors()];
		for (int i = 0; i < preimageWorkers.length; i++) {
			preimageWorkers[i] = new Thread(new PreimageWorker(generator, n, hashByte, state));
			preimageWorkers[i].start();
		}
		
		for (Thread worker: preimageWorkers) {
			try {
				worker.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		return state.getPreimageResult();
	}

}
