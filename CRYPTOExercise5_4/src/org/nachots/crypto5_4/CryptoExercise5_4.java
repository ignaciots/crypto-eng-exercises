package org.nachots.crypto5_4;

import org.nachots.crypto5_4.preimage.Preimage;
import org.nachots.crypto5_4.preimage.PreimageResult;

/**
 * Cryptography Engineering 5.4 exercise
 * @author nachots
 * @version 1.0
 */
public class CryptoExercise5_4 {
	
	private static final int NUMBER_OF_RUNS = 5; // number of runs for each SHA-512-N preimage variation

	public static void main(String[] args) {
		cryptoExercise5_4();
	}
	
	private static void cryptoExercise5_4() {
		System.out.println("---Exercise 5.4---");
		System.out.println();
		
		// Test preimage for SHA-512-8
		System.out.println("SHA-512-8");
		testSHA512PreimageFor(8, new byte[] {(byte) 0xa9});
		System.out.println();
		
		// Test preimage for SHA-512-16
		System.out.println("SHA-512-16");
		testSHA512PreimageFor(16, new byte[] {0x3d, 0x4b});
		System.out.println();
		
		// Test preimage for SHA-512-24
		System.out.println("SHA-512-24");
		testSHA512PreimageFor(24, new byte[] {0x3a, 0x7f, 0x27});
		System.out.println();
		
		// Test preimage for SHA-512-32
		System.out.println("SHA-512-32");
		testSHA512PreimageFor(32, new byte[] {(byte) 0xc3, (byte) 0xc0, 0x35, 0x7c});
		System.out.println();
	}
	
	/**
	 * Test for SHA-512-n preimage
	 * @param n The number of bits in the SHA-512-n function.
	 * @param hash the hash used to find the preimage.
	 */
	private static void testSHA512PreimageFor(int n, byte[] hash) {
		long totalTime = 0;
		for (int i = 0; i < NUMBER_OF_RUNS; i++) {
			long initTime = System.currentTimeMillis();
			PreimageResult result = Preimage.findSHA512PreimageFor(n, hash);
			long endTime = System.currentTimeMillis();
			long currentTime = (endTime - initTime);
			totalTime += currentTime;
			System.out.println("Preimage found in " + currentTime + " ms.");
			System.out.println(result);
		}
		
		long averageTime = totalTime / NUMBER_OF_RUNS;
		System.out.println("Average time for " + NUMBER_OF_RUNS + " runs is " + averageTime + " ms.");
	} 
}
