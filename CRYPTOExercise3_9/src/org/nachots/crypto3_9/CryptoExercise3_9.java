package org.nachots.crypto3_9;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.nachots.crypto3_9.aes.AESCipher;
import org.nachots.cryptoutils.CryptoUtils;

/**
 * Exercise 3.9 main class
 * @author nachots
 * @version 1.0
 */
public class CryptoExercise3_9 {
	
	public static void main(String[] args) {
		System.out.println("---Exercise 3.9---");
		System.out.println();
		
		final byte[] plaintext = {0x29, (byte) 0x6c,(byte) 0x93, (byte) 0xfd, (byte) 0xf4, (byte) 0x99, (byte) 0xaa, (byte) 0xeb,
									(byte) 0x41, (byte) 0x94, (byte) 0xba, (byte) 0xbc, (byte) 0x2e, (byte) 0x63, (byte) 0x56, 0x1d};
		
		final byte[] key = {(byte) 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
								0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01};
		
		byte[] ciphertext = null;
		try {
			ciphertext = AESCipher.cipher(plaintext, key);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		
		System.out.println("Plaintext:");
		CryptoUtils.printByteArray(plaintext, 16);
		System.out.println("Key:");
		CryptoUtils.printByteArray(key, 16);
		System.out.println("Ciphertext:");
		CryptoUtils.printByteArray(ciphertext, 16);
	}

}
