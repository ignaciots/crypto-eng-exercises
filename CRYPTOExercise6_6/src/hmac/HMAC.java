package hmac;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * HMAC helper class.
 * @author nachots
 * @version 1.0
 */
public class HMAC {
	
	
	public static byte[] mac(byte[] message, byte[] key) throws NoSuchAlgorithmException, InvalidKeyException {
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(new SecretKeySpec(key, "AES"));
		return mac.doFinal(message);
	}

}
