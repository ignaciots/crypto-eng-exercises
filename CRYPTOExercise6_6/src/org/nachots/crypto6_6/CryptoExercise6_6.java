package org.nachots.crypto6_6;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import org.nachots.cryptoutils.CryptoUtils;

import hmac.HMAC;

/**
 * Exercise 6.6 main class
 * @author nachots
 * @version 1.0
 */
public class CryptoExercise6_6 {
	
	public static void main(String[] args) {
		System.out.println("---Exercise 6.6---");
		System.out.println();
		
		final byte[] message = {0x4d, 0x41, 0x43, 0x73, 0x20, 0x61, 0x72, 0x65, 0x20, 0x76, 0x65, 0x72, 0x79, 0x20, 0x75, 0x73,
									0x65, 0x66, 0x75, 0x6c, 0x20, 0x69, 0x6e, 0x20, 0x63, 0x72, 0x79, 0x70, 0x74, 0x6f, 0x67, 0x72,
									0x61, 0x70, 0x68, 0x79, 0x21};
		
		final byte[] key = new byte[32];
		for (int i = 0; i < key.length; i++) {
			key[i] = 0x0b;
		}
		
		System.out.println("Message:");
		CryptoUtils.printByteArray(message, 16);
		System.out.println("Plantext (text):");
		CryptoUtils.printByteText(message);
		System.out.println();
		System.out.println("Key:");
		CryptoUtils.printByteArray(key, 16);
		
		byte[] mac = null;
		
		try {
			mac = HMAC.mac(message, key);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		
		System.out.println("MAC tag:");
		CryptoUtils.printByteArray(mac, 16);
	}

}
