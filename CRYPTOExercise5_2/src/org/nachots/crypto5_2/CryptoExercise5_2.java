package org.nachots.crypto5_2;

import static org.nachots.cryptoutils.CryptoUtils.printByteArray;
import static org.nachots.cryptoutils.CryptoUtils.printByteText;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Exercise 5.2 main class.
 * @author nachots
 * @version 1.0
 */
public class CryptoExercise5_2 {
	
	private static final String DIGEST_ALGORITHM = "SHA-512";

	public static void main(String[] args) {
		cryptoExercise5_2();
	}
	
	private static void cryptoExercise5_2() {
		System.out.println("---Exercise 5.2---");
		System.out.println();
		
		final byte[] message = {0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x2c, 0x20, 0x77,
								0x6f, 0x72, 0x6c, 0x64, 0x2e, 0x20, 0x20, 0x20};
		System.out.println("Message (hex):");
		printByteArray(message, 8);
		System.out.println("Message (text):");
		printByteText(message);
		
		byte[] digest = null;
		try {
			MessageDigest sha512 = MessageDigest.getInstance(DIGEST_ALGORITHM);
			digest = sha512.digest(message);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		System.out.println();
		System.out.println();
		System.out.println("Digest (hex):");
		printByteArray(digest, 8);
	}
}
